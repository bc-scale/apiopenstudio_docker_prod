ARG ALPINE_VERSION=3.16

FROM alpine:${ALPINE_VERSION} as apiopenstudio_stage_1

# Keys to git clone with
ARG SSH_PRIVATE_KEY
ARG CI_REPOSITORY
ARG BRANCH
ARG TAG

# Install packages and remove default server definition
RUN apk add --no-cache \
  curl \
  git \
  mysql-client \
  nginx \
  openssh-client \
  openssl \
  php81 \
  php81-ctype \
  php81-curl \
  php81-dom \
  php81-fpm \
  php81-gd \
  php81-intl \
  php81-mbstring \
  php81-mysqli \
  php81-opcache \
  php81-openssl \
  php81-phar \
  php81-session \
  php81-simplexml \
  php81-sodium \
  php81-tokenizer \
  php81-xml \
  php81-xmlreader \
  php81-xmlwriter \
  supervisor

# Create symlink so programs depending on `php` still function
RUN ln -s /usr/bin/php81 /usr/bin/php

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php81/php-fpm.d/www.conf
COPY config/php.ini /etc/php81/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Add ssh capabilities
RUN mkdir $HOME/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > $HOME/.ssh/id_rsa
RUN chmod 600 $HOME/.ssh/id_rsa

# Add composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add application
WORKDIR /root/temp
RUN ssh-keyscan gitlab.com > $HOME/.ssh/known_hosts
RUN git clone $CI_REPOSITORY
WORKDIR /root/temp/apiopenstudio
# Checkout the branch or tag passed from parent pipeline
RUN if [[ -z "$BRANCH" ]] && [[ ! -z "$TAG" ]] ; \
    then git checkout $TAG -b latest ; \
    elif [[ ! -z "$BRANCH" ]] ; \
    then git checkout $BRANCH ; \
    else exit 1 ; \
    fi
RUN rm -r .git*
RUN composer install --no-dev

# Stage 2
FROM apiopenstudio_stage_1

WORKDIR /var/www/html/

RUN mkdir -p /etc/nginx/certs/

RUN touch /var/log/nginx/db.log /var/www/html/jwt.key /var/www/html/jwt.key.pub

RUN chmod 600 /var/www/html/jwt.key*

# ensure nobody has the correct permissions and access
RUN chown -R nobody.nobody /var/www/html /run /var/lib/nginx /var/log/nginx

# Copy the application to the docroot and delete the original abd composer files
COPY --from=apiopenstudio_stage_1 --chown=nobody /root/temp/apiopenstudio .
RUN rm -r /root/temp /root/.composer

# Switch to use a non-root user from here on
USER nobody

# Expose the port nginx is reachable on
EXPOSE 80
EXPOSE 443

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
