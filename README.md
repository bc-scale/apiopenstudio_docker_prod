# naala89/apiopenstudio

This image provides a docker image for production use ApiOpenStudio.

It contains:

* Nginx
* PHP 8.1
* The tagged version of ApiOpenStudio

## Getting Started

This container contains a full working Nginx/PHP server and ApiOpenStudio. It
requires a separate MariaDB container, and can be quickly installed on a
bare-bones production server in minutes to have a fully functioning
ApiOpenStudio instance.

**Note:** There should not be a running instance of Apache or Nginx on the
server, otherwise the incoming port 80/443 requests will not get directed to
the Docker container.

## Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

## Usage

### Useful File Locations

* `/etc/nginx/nginx.conf` - The main Nginx config file.
* `/etc/php81/php-fpm.d/www.conf` - www pool config file.
* `/etc/php81/conf.d/custom.ini` - PHP configuration.
* `/etc/supervisor/conf.d/supervisord.conf` - supervisor configuration.

### Example

```shell
# Create the database volume
$ sudo docker volume create dbdata

# Create the Docker network
$ sudo docker network create api_network

# Start the database container
$ sudo docker run -d --name=apiopenstudio-db \
-e MYSQL_DATABASE=apiopenstudio \
-e MYSQL_USER=apiopenstudio \
-e MYSQL_PASSWORD=my_s3cr3t \
-e MYSQL_ROOT_PASSWORD=my_s3cr3t \
-h apiopenstudio-db \
-v dbdata:/var/lib/mysql \
--restart=unless-stopped \
--network=api_network \
mariadb:latest

# Download and configure the settings file.
$ sudo mkdir -p /path/to
$ sudo wget -O /path/to/settings.yml https://raw.githubusercontent.com/naala89/apiopenstudio/master/example.settings.yml

# Start the API container
$ sudo docker run -d --name apiopenstudio-api \
-p 80:80 \
-p 443:443 \
--mount type=bind,source=/path/to/settings.yml,target=/var/www/html/settings.yml \
--mount type=bind,source=/path/to/ssl.crt,target=/etc/nginx/certs/apiopenstudio.crt \
--mount type=bind,source=/path/to/ssl.key,target=/etc/nginx/certs/apiopenstudio.key \
--network=api_network \
--restart=unless-stopped \
naala89/apiopenstudio:tag

# SSH into the API container
$ sudo docker exec -it apiopenstudio-api sh

# Configure the database
$ ./bin/aos-install
```

### Detailed instructions

Visit the [ApiOpenStudio wiki][wiki] for full instructions and configuration of
`settings.yml`

# Find Us

* [GitLab][gitlab_apiopenstudio]
* [GitHub][github_naala89]
* [www.apiopenstudio.com][web_apiopenstudio]

# Versioning

This image automatically built from tags on the master branch at
[GitLab][gitlab_apiopenstudio].

Version tags (i.e. `naala89/apiopenstudio:1.0`) references the version of the
image and the tag of the release.

Version tags of `master` & `develop` (i.e. `naala89/apiopenstudio:master`)
references the latest version on the branch.

The `latest` tag is most recent built image and should not be used to pull the
latest tagged version.

# Authors

* **John Avery** - [John - GitLab][gitlab_john]

# License

This project is licensed under the MIT License - see the [LICENSE.md][license]
file for details.

# Acknowledgments

Many thanks to:

* [trafex][trafex] for creating the great [trafex/php-nginx][trafex_php_nginx]
  image that inspired the architecture of this image.

[docker_windows]: https://docs.docker.com/desktop/install/windows-install/

[docker_osx]: https://docs.docker.com/desktop/install/mac-install/

[docker_linux]: https://docs.docker.com/desktop/install/linux-install/

[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio/apiopenstudio

[gitlab_john]: https://gitlab.com/john89

[github_naala89]: https://github.com/naala89

[web_apiopenstudio]: https://www.apiopenstudio.com

[license]: https://gitlab.com/apiopenstudio/docker_images/apiopenstudio_docker_prod/-/blob/master/LICENSE.md

[trafex]: https://hub.docker.com/u/trafex

[trafex_php_nginx]: https://hub.docker.com/r/trafex/php-nginx

[wiki]: https://wiki.apiopenstudio.com/install/docker/production
